﻿using csvDeserializer;

namespace csvDeserializerTest;

[TestClass]
public class UnitTest1
{
    [TestMethod]
    public void DeserializeCsvToObjectList_ValidCsv_ReturnsCorrectCount()
    {
        // Arrange
        string csvFilePath = "/Users/sidesarr/Documents/NetCore/Agiltoo/people-100.csv";
        int expectedCount = 100;

        // Act
        List<People> deserializedObjects = Utils.DeserializeCsvToObjectList(csvFilePath);

        // Assert
        Assert.AreEqual(expectedCount, deserializedObjects.Count);
    }
}
