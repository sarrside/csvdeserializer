﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;


namespace csvDeserializer
{
	public class Utils
	{

        /// <summary>
        /// <<Désérialise un fichier CSV en une liste d'objets de type People.>>
        /// </summary>
        /// <param name="csvFilePath">Le chemin d'accès du fichier CSV à désérialiser.</param>
        /// <returns>Une liste d'objets de type People.</returns>
        public static List<People> DeserializeCsvToObjectList(string csvFilePath)
        {
            using (var reader = new StreamReader(csvFilePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<People>();
                return new List<People>(records);   
            }
        }

    }
}

