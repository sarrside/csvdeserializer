﻿using System;
using CsvHelper.Configuration.Attributes;

namespace csvDeserializer
{
	public class People
	{
        [Name("Index")]
        public int Index { get; set; }
        [Name("User Id")]
        public string UserId { get; set; } = string.Empty;
        [Name("First Name")]
        public string FirstName { get; set; } = string.Empty;
        [Name("Last Name")]
        public string LastName { get; set; } = string.Empty;
        [Name("Sex")]
        public string Sex { get; set; } = string.Empty;
        [Name("Email")]
        public string Email { get; set; } = string.Empty;
        [Name("Phone")]
        public string Phone { get; set; } = string.Empty;
        [Name("Date of birth")]
        public string Dob { get; set; } = string.Empty;
        [Name("Job Title")]
        public string JobTitle { get; set; } = string.Empty;

    }
}

